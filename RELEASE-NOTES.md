# v1.1 - April 1, 2020
* [bugfix] Don't let long comments resize the user interface (@dagurval)
* [bugfix] Improve wallet compatibility checks and add test coverage (@dagurval)
* [feature] Add the ability to cancel all pledges (@emergent-reasons)
* [feature] Memorize pledges made, so they can be cancelled later (@emergent-reasons)
* [feature] Message to explicitly notify users to copy the pledge back to the website (@emergent-reasons)
* [feature] Add menu button & about dialog (@dagurval)
* [feature] Add status bar to give user feedback on error (@dagurval)
* [feature] Always select entire pledge in 'Copy pledge' text box (@dagurval)

# v1.0 - March 9, 2020
* First release
