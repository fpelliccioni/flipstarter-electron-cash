import unittest
from .versionutil import parse_package_version, is_compatible_version

class TestVersionUtil(unittest.TestCase):
    def test_nomalize_version(self):
        self.assertEqual(parse_package_version("4.0"), (4, ''))
        self.assertEqual(parse_package_version("3.3.0"), (3, 3, ''))
        self.assertEqual(parse_package_version("4.0.11"), (4, 0, 11, ''))
        self.assertEqual(parse_package_version('3.4.5_iOS'), (3, 4, 5, '_iOS'))
        self.assertEqual(parse_package_version('3.3.5'), (3, 3, 5, ''))
        self.assertEqual(parse_package_version('3.3'), (3, 3, ''))
        self.assertEqual(parse_package_version('3.3.0'), (3, 3, ''))
        self.assertEqual(parse_package_version('   3.2.2.0 ILikeSpaces '), (3, 2, 2, 'ILikeSpaces'))
        self.assertEqual(parse_package_version('3.3.0'), (3, 3, ''))
        self.assertEqual(parse_package_version('3.5.0.0.0'), (3, 5, ''))
        self.assertEqual(parse_package_version('3.5.0.0.0_iOS'), (3, 5, '_iOS'))

    def test_is_compatible_version(self):
        self.assertTrue(is_compatible_version("4.0.10"))
        self.assertTrue(is_compatible_version("4.0.11"))
        self.assertTrue(is_compatible_version("5.0"))
        self.assertTrue(not is_compatible_version("4.0.11_iOS"))
        self.assertTrue(not is_compatible_version("4.0.11 ILikeSpaces"))
