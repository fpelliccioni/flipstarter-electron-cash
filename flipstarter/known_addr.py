import time

from electroncash.i18n import _

AUTO_EXPIRE = 3600 * 24 * 30 * 6

# Addresses that are known to be a part of future flipstarter campaingn at the
# time of the release of this plugin.
#
# This list needs to be kept up-to-date or deprecated. Projects dissapear,
# keys get compromised. Any addresses not re-verified expire after 6 months.
#
# Addresses not planned in a future flipstarter campaign should not be
# re-verified.
KNOWN_ADDRESSES = {
    # For initial node campaigns
    "bitcoincash:pqy73mq5emhzjcs87na46zmrl0wnpx5wmudp2swzzy": {
        "owners": "Bitcoin ABC",
        "added": 1586260932,
    },
    "bitcoincash:prnc2exht3zxlrqqcat690tc85cvfuypngh7szx6mk": {
        "owners": "Bitcoin Cash Node",
        "added": 1586260932,
    },
    "bitcoincash:qp0sg3l642sy0gxs6hcm4thlcxq6ff6t6glpd0t6fv": {
        "owners": "BCHD",
        "added": 1586260932,
    },
    "bitcoincash:qqzkk06w4k3ye05q7fly566tan2chfkerqgku569zg": {
        # this is Verde's legacy vanity address 1VerdeMuXH1ApSZsQDkuHHZrwJaAtXTVn
        "owners": "Bitcoin Verde",
        "added": 1586260932,
    },
    "bitcoincash:qphdx5qyr7txp27jtfpu8kx6nm33udz3hyvdwz8dn8": {
        "owners": "Knuth",
        "added": 1586260932,
    },

    # For flipstarter
    "bitcoincash:pq2pn6aj5stt6k9pk7amz73hfgyp0sjqlsg69ed5yt": {
        "owners": "Flipstarter",
        "added": 1586260932,
    },

    # For beta user testing
    "bitcoincash:qr4aadjrpu73d2wxwkxkcrt6gqxgu6a7usxfm96fst": {
        'owners': "Flipstarter TESTING", 'added': 1585662661
    },

    # For automated tests
    "bitcoincash:dummy1": {
        'owners': "DUMMY 1",
        'added': 2000000000
    },
    "bitcoincash:dummy2": {
        'owners': "DUMMY 2",
        'added': 2000000000
    },
    "bitcoincash:dummy3": {
        'owners': "DUMMY 3",
        'added': 2000000000
    }
}

def get_addr_owner(addr, now = None):

    if not addr in KNOWN_ADDRESSES:
        return None

    if now is None:
        now = int(time.time())

    meta = KNOWN_ADDRESSES[addr]

    # Cannot be first known in the future.
    if meta['added'] > now:
        return None

    # Not verified in a long time
    if now > meta['added'] + AUTO_EXPIRE:
        return None

    return meta['owners']

WARN = "⚠️"
def summarize_outputs(addrs, now = None):

    if len(addrs) == 0:
        raise Exception(_("Invalid pledge outputs"))

    owners = []

    for a in addrs:
        owner = get_addr_owner(a, now)
        if owner is None:
            return "{} {} {}\n\n{}".format(
                WARN,
                _("Pledge has a destination unknown to this plugin."),
                WARN,
                _("See FAQ on Flipstarter website for more information."))

        owners.append(owner)

    if len(owners) == 1:
        return _("This pledge appears to be for {}.").format(owners[0])

    if len(owners) == 2:
        return _("This pledge appears to be for {} and {}.").format(
                owners[0], owners[1])

    return "{} {} {}\n\n{}".format(
            WARN,
            _("Pledge appears to be to multiple destinations."),
            WARN,
            _("See FAQ on Flipstarter website for more information."))
